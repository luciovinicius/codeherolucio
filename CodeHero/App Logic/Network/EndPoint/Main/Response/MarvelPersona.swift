//
//  MarvelPersona.swift
//  CodeHero
//
//  Created by Lucio Couto on 25/09/19.
//  Copyright © 2019 Lucio Couto. All rights reserved.
//

import Foundation

struct MarvelPersona: Codable {
    let id: Int
    let name, marvelPersonaDescription: String
    let thumbnail: Thumbnail
    
    enum CodingKeys: String, CodingKey {
        case id, name, thumbnail
        case marvelPersonaDescription = "description"
    }
}

struct Thumbnail: Codable {
    let path: String
    let thumbnailExtension: String
    
    enum CodingKeys: String, CodingKey {
        case path
        case thumbnailExtension = "extension"
    }
    
    var urlMedium: String {
        get {
            return "\(path)/standard_medium.\(thumbnailExtension)"
        }
    }
    
    var urlLarge: String {
        get {
            return "\(path)/standard_fantastic.\(thumbnailExtension)"
        }
    }
}
