//
//  MarvelPersonaParameters.swift
//  CodeHero
//
//  Created by Lucio Couto on 25/09/19.
//  Copyright © 2019 Lucio Couto. All rights reserved.
//

import Foundation

public struct MarvelPersonaParameters: RequestParameters {
    var apikey: String
    var limit: Int
    var offset: Int
    var ts: String
    var hash: String
}

public struct MarvelPersonaParametersSearch: RequestParameters {
    var apikey: String
    var limit: Int
    var offset: Int
    var ts: String
    var hash: String
    var nameStartsWith: String
}

