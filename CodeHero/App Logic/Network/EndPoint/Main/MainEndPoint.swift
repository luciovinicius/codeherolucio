//
//  MainEndPoint.swift
//  CodeHero
//
//  Created by Lucio Couto on 25/09/19.
//  Copyright © 2019 Lucio Couto. All rights reserved.
//

import Foundation
import Alamofire

public enum MainEndPoint {
    case characters(parameters: MarvelPersonaParameters)
    case charactersSearch(parameters: MarvelPersonaParametersSearch)
}

extension MainEndPoint: EndPointType {
    var service: NetworkService {
        return .main
    }
    
    var path: String {
        switch self {
        case .characters:
            return "characters"
        case .charactersSearch:
            return "characters"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .characters:
            return .get
        case .charactersSearch:
            return .get
        }
        
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
    
    var parameters: Parameters? {
        switch self {
        case .characters(let parameters):
            return parameters.toParameters()
        case .charactersSearch(let parameters):
            return parameters.toParameters()
        }
    }
    
    var encoding: ParameterEncoding {
        switch self {
        case .characters:
            return URLEncoding.default
        case .charactersSearch:
            return URLEncoding.default
        }
    }
    
    var name: String {
        return String(describing: self)
    }

}
