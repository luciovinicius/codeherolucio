//
//  MarvelResponse.swift
//  CodeHero
//
//  Created by Lucio Couto on 25/09/19.
//  Copyright © 2019 Lucio Couto. All rights reserved.
//

import Foundation

struct MarvelResponse<T>: Codable where T: Codable {
    let data: MarvelResponseData<T>?
}

struct MarvelResponseData<T>: Codable where T: Codable {
    let offset, limit, total, count: Int?
    let results: [T]?
}
