//
//  NetworkManager.swift
//  CodeHero
//
//  Created by Lucio Couto on 25/09/19.
//  Copyright © 2019 Lucio Couto. All rights reserved.
//

import Foundation

struct NetworkManager {

    static let environment: NetworkEnvironment = .production

    let routerMain = Router<MainEndPoint>()
}
