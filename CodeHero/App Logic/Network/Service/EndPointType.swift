//
//  EndPointType.swift
//  CodeHero
//
//  Created by Lucio Couto on 25/09/19.
//  Copyright © 2019 Lucio Couto. All rights reserved.
//

import Foundation
import Alamofire

protocol EndPointType {
    var service: NetworkService { get }

    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var headers: HTTPHeaders? { get }
    var parameters: Parameters? { get }
    var encoding: ParameterEncoding { get }
    var name: String { get }
}
