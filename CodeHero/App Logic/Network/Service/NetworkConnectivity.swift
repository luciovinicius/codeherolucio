//
//  NetworkConnectivity.swift
//  CodeHero
//
//  Created by Lucio Couto on 25/09/19.
//  Copyright © 2019 Lucio Couto. All rights reserved.
//

import Foundation
import Alamofire

struct NetworkConnectivity {
    static let sharedInstance = NetworkReachabilityManager()!
    static var isConnectedToInternet: Bool {
        return self.sharedInstance.isReachable
    }
}
