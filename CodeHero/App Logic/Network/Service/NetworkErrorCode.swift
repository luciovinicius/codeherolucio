//
//  NetworkErrorCode.swift
//  CodeHero
//
//  Created by Lucio Couto on 25/09/19.
//  Copyright © 2019 Lucio Couto. All rights reserved.
//

import Foundation

enum NetworkErrorCode: Int {

    // MARK: HTTP ERROR's
    case authenticationError = 401
    case badRequest = 400
    case conflict = 409
    case gone = 410

    // MARK: Others Error's Range NSCoderErrorMinimum(4864) - NSCoderErrorMaximum(4991)

    case failed = 4864
    case noData = 4865
    case unableToDecode = 4866
    case internetConectionFailed = 4867

}
