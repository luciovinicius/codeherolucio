//
//  Router.swift
//  CodeHero
//
//  Created by Lucio Couto on 25/09/19.
//  Copyright © 2019 Lucio Couto. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit

protocol NetworkRouter: class {
    associatedtype EndPoint: EndPointType
}

class Router<EndPoint: EndPointType>: NetworkRouter {

    func requestList<T: Codable>(from route: EndPoint, responseType: T.Type) -> Promise<[T]> {

        return Promise { promise in

            var headers: [String: String] = route.sharedHeader
            if let headersRequest = route.headers {
                headers.update(other: headersRequest)
            }

            Alamofire.request(route.baseURL.appendingPathComponent(route.path), method: route.httpMethod, parameters: route.parameters, encoding: route.encoding, headers: headers)
                .responseJSON { (data) in

                    guard let statusCode = data.response?.statusCode else {
                        promise.reject(NSError(domain: "NetworkRouter", code: NetworkErrorCode.internetConectionFailed.rawValue, userInfo: nil))
                        return
                    }
                    switch statusCode {
                    case 200...204:
                        if let value = data.result.value {
                            do {
                                let response = try CodableParser<T>.parse(dict: value)
                                promise.fulfill([response])
                            } catch {
                                do {
                                    let responseList = try CodableParser<T>.parse(list: value)
                                    promise.fulfill(responseList)
                                } catch {
                                    promise.reject(NSError(domain: "NetworkRouter", code: NetworkErrorCode.unableToDecode.rawValue, userInfo: nil))
                                }
                            }
                        } else {
                            do {
                                let response = try CodableParser<T>.parse(dict: [:])
                                promise.fulfill([response])
                            } catch {
                                promise.reject(NSError(domain: "NetworkRouter", code: NetworkErrorCode.unableToDecode.rawValue, userInfo: nil))
                            }
                        }
                        return
                    default:
                        promise.reject(NSError(domain: "NetworkRouter", code: NetworkErrorCode.failed.rawValue, userInfo: nil))
                    }
                    promise.reject(NSError(domain: "NetworkRouter", code: NetworkErrorCode.failed.rawValue, userInfo: nil))
                    return
            }
        }
    }

    func request<T: Codable>(from route: EndPoint, responseType: T.Type) -> Promise<T> {
        let aux = requestList(from: route, responseType: responseType)
        return Promise { promise in
            aux.done({ response in
                if let object = response.first {
                    promise.fulfill(object)
                } else {
                    promise.reject(NSError(domain: "NetworkRouter", code: NetworkErrorCode.unableToDecode.rawValue, userInfo: nil))
                }
            }).catch({error in
                promise.reject(error)
            })
        }
    }

}
