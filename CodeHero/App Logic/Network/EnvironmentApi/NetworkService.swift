//
//  NetworkService.swift
//  CodeHero
//
//  Created by Lucio Couto on 25/09/19.
//  Copyright © 2019 Lucio Couto. All rights reserved.
//

import Foundation

enum NetworkService {
    case main
    
    var path: String {
        switch self {
        case .main:
            return "\(apiVersion)/public"
        }
    }
    
    var apiVersion: String {
        switch self {
        case .main:
            return "v1"
        }
    }
}
