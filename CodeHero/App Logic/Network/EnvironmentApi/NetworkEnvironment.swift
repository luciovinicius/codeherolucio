//
//  NetworkEnvironment.swift
//  CodeHero
//
//  Created by Lucio Couto on 25/09/19.
//  Copyright © 2019 Lucio Couto. All rights reserved.
//

import Foundation

enum NetworkEnvironment {
    case production
}

extension EndPointType {
    
    var environmentBaseURL: String {
        switch NetworkManager.environment {
        case .production:
            return endPoint + service.path
        }
    }

    var baseURL: URL {
        guard let url = URL(string: environmentBaseURL) else { fatalError("baseURL could not be configured.")}
        return url
    }

    var sharedHeader: [String: String] {
        get {
            var headers: [String: String] = [:]
            return headers
        }
    }
}
