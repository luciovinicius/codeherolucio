//
//  CodableParser.swift
//  CodeHero
//
//  Created by Lucio Couto on 25/09/19.
//  Copyright © 2019 Lucio Couto. All rights reserved.
//

import Foundation

class CodableParser<T: Codable> {

    static func parse(dict: Any?) throws -> T {
        guard let safeDict = dict as? [String: Any] else {
            throw NSError(domain: "CodableParser", code: -1,
                          userInfo: [NSLocalizedDescriptionKey: "Can't parse nil dictionary"])
        }
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: safeDict, options: .prettyPrinted)
            let value = try JSONDecoder().decode(T.self, from: jsonData)
            return value
        } catch let error {
            throw error
        }
    }

    static func parse(list: Any?) throws -> [T] {
        guard let safeList = list as? [[String: Any]] else {
            throw NSError(domain: "CodableParser", code: -1,
                          userInfo: [NSLocalizedDescriptionKey: "Can't parse nil list"])
        }
        return try safeList.compactMap({
            do {
                return try CodableParser.parse(dict: $0)
            } catch let error {
                throw error
            }
        })
    }

}
