//
//  RequestParameters.swift
//  CodeHero
//
//  Created by Lucio Couto on 24/09/19.
//  Copyright © 2019 Lucio Couto. All rights reserved.
//

import Foundation

public protocol RequestParameters {
    func toParameters() -> [String: Any]
}

public extension RequestParameters {
    
    func toParameters() -> [String: Any] {
        var params: [String: Any] = [:]
        let mirror = Mirror(reflecting: self)
        mirror.children.forEach { (key, value) in
            guard let safeKey = key else { return }
            params[safeKey] = value
        }
        return params
    }
    
}
