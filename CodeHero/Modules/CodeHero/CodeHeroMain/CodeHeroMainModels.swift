//
//  CodeHeroMainModels.swift
//  CodeHero
//
//  Created by Lucio Couto on 24/09/19.
//  Copyright (c) 2019 Lucio Couto. All rights reserved.
//

import UIKit


enum CodeHeroMain {
    
    enum MarvelCharacter {
        
        struct Request {
            var page: Int
            var name: String
        }
        
        enum Response {
            struct Error {
            }
            
            struct Success {
                var characters: [MarvelPersona]
                var totalPages: Int
                var pageSelected: Int
            }
        }
        enum ViewModel {
            struct Error {
            }
            struct Success {
                var characters: [MarvelPersona]
                var totalPages: Int
                var pageSelected: Int
            }
        }
    }
    
    enum MarvelCharacterSave {
        
        struct Request {
            var character: MarvelPersona
        }
        
        enum Response {
            struct Error {
            }
            
            struct Success {
            }
        }
        enum ViewModel {
            struct Error {
            }
            struct Success {
            }
        }
    }
}
