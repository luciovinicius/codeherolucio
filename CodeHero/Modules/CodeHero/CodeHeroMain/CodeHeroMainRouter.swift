//
//  CodeHeroMainRouter.swift
//  CodeHero
//
//  Created by Lucio Couto on 24/09/19.
//  Copyright (c) 2019 Lucio Couto. All rights reserved.
//

import UIKit


@objc protocol CodeHeroMainRoutingLogic {
    func routeTopresentDetails(segue: UIStoryboardSegue?)
}


protocol CodeHeroMainDataPassing {
    var dataStore: CodeHeroMainDataStore? { get }
}


class CodeHeroMainRouter: NSObject, CodeHeroMainRoutingLogic, CodeHeroMainDataPassing {
    
    
    // Var's
    weak var viewController: CodeHeroMainViewController?
    var dataStore: CodeHeroMainDataStore?
    
    func routeTopresentDetails(segue: UIStoryboardSegue?) {
        if let destinationVC = segue?.destination as? CodeHeroMainDetailViewController {
            if var destinationDS = destinationVC.router?.dataStore, let sourceDS = self.dataStore {
                passDataToDetail(source: sourceDS, destination: &destinationDS)
            }
        }
    }
  
    func passDataToDetail(source: CodeHeroMainDataStore, destination: inout CodeHeroMainDetailDataStore) {
        destination.persona = source.persona
    }
}
