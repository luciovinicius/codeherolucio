//
//  CodeHeroMainPresenter.swift
//  CodeHero
//
//  Created by Lucio Couto on 24/09/19.
//  Copyright (c) 2019 Lucio Couto. All rights reserved.
//

import UIKit


protocol CodeHeroMainPresentationLogic {
    func presentCharacters(response: CodeHeroMain.MarvelCharacter.Response.Success)
    func presentCharacters(response: CodeHeroMain.MarvelCharacter.Response.Error)
}


class CodeHeroMainPresenter: CodeHeroMainPresentationLogic {
  
    
    // Var's
    weak var viewController: CodeHeroMainDisplayLogic?
    
    func presentCharacters(response: CodeHeroMain.MarvelCharacter.Response.Success) {
        let viewModel = CodeHeroMain.MarvelCharacter.ViewModel.Success(characters: response.characters, totalPages: response.totalPages, pageSelected: response.pageSelected)
        viewController?.displayCharacters(viewModel: viewModel)
    }
    
    func presentCharacters(response: CodeHeroMain.MarvelCharacter.Response.Error) {
        let viewModel = CodeHeroMain.MarvelCharacter.ViewModel.Error()
        viewController?.displayCharacters(viewModel: viewModel)
    }
}
