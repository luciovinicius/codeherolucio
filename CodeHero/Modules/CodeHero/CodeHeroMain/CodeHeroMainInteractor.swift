//
//  CodeHeroMainInteractor.swift
//  CodeHero
//
//  Created by Lucio Couto on 24/09/19.
//  Copyright (c) 2019 Lucio Couto. All rights reserved.
//

import UIKit


protocol CodeHeroMainBusinessLogic {
    func getCharacters(request: CodeHeroMain.MarvelCharacter.Request)
    func saveCharacter(request: CodeHeroMain.MarvelCharacterSave.Request)
}


protocol CodeHeroMainDataStore {
    var persona: MarvelPersona? {get set}
}


class CodeHeroMainInteractor: CodeHeroMainBusinessLogic, CodeHeroMainDataStore {
    
    var persona: MarvelPersona?
    
    // Var's
    var presenter: CodeHeroMainPresentationLogic?
    var worker = CodeHeroMainWorker()
  
    func getCharacters(request: CodeHeroMain.MarvelCharacter.Request) {
        
        let limit = 4
        let offset = request.page * limit
        
        worker.getCharacter(limit: limit, offset: offset, name: request.name)
            .done({ marvelResponse in
                let response = CodeHeroMain.MarvelCharacter.Response.Success(characters: marvelResponse.data?.results ?? [MarvelPersona](),
                                                                             totalPages: (marvelResponse.data?.total ?? 4) / 4,
                                                                             pageSelected: request.page + 1)
                self.presenter?.presentCharacters(response: response)
            }).catch({ error in
                self.presenter?.presentCharacters(response: CodeHeroMain.MarvelCharacter.Response.Error())
            })
        
    }
    
    func saveCharacter(request: CodeHeroMain.MarvelCharacterSave.Request) {
        persona = request.character
    }
}
