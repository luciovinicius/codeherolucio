//
//  CodeHeroMainConfigurator.swift
//  CodeHero
//
//  Created by Lucio Couto on 24/09/19.
//  Copyright (c) 2019 Lucio Couto. All rights reserved.
//

import UIKit


extension CodeHeroMainViewController {
  
    
    // Setup
    func setup() {
   
        let viewController          = self
        let interactor              = CodeHeroMainInteractor()
        let presenter               = CodeHeroMainPresenter()
        let router                  = CodeHeroMainRouter()
    
        viewController.interactor   = interactor
        viewController.router       = router
        interactor.presenter        = presenter
        presenter.viewController    = viewController
        router.viewController       = viewController
        router.dataStore            = interactor
    }
  
    
    // Routing
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
  
    
}
