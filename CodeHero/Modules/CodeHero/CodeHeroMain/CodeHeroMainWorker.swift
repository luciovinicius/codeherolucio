//
//  CodeHeroMainWorker.swift
//  CodeHero
//
//  Created by Lucio Couto on 24/09/19.
//  Copyright (c) 2019 Lucio Couto. All rights reserved.
//

import UIKit
import PromiseKit


class CodeHeroMainWorker {
   
    let network = NetworkManager()

       
    func getCharacter(limit: Int, offset: Int, name: String) -> Promise<MarvelResponse<MarvelPersona>> {
        
        let ts = "\((Date().timeIntervalSince1970 * 1000).rounded())".replacingOccurrences(of: ".0", with: "")
        let hash = "\(ts)\(privateKey)\(publicKey)".MD5Hash()
        if name == "" {
            let parameters = MarvelPersonaParameters(apikey: publicKey,limit: limit, offset: offset, ts: ts, hash: hash)
            return network.routerMain.request(from: .characters(parameters: parameters), responseType: MarvelResponse<MarvelPersona>.self)
        } else {
            let parameters = MarvelPersonaParametersSearch(apikey: publicKey,limit: limit, offset: offset, ts: ts, hash: hash, nameStartsWith: name)
            return network.routerMain.request(from: .charactersSearch(parameters: parameters), responseType: MarvelResponse<MarvelPersona>.self)
        }
    }
}
