//
//  CodeHeroMainDetailInteractor.swift
//  CodeHero
//
//  Created by Lucio Couto on 28/09/19.
//  Copyright (c) 2019 Lucio Couto. All rights reserved.
//

import UIKit


protocol CodeHeroMainDetailBusinessLogic {
    func getCharacter(request: CodeHeroMainDetail.MarvelCharacterSave.Request)
}


protocol CodeHeroMainDetailDataStore {
    var persona: MarvelPersona? {get set}
}


class CodeHeroMainDetailInteractor: CodeHeroMainDetailBusinessLogic, CodeHeroMainDetailDataStore {
    
    var persona: MarvelPersona?
    
    // Var's
    var presenter: CodeHeroMainDetailPresentationLogic?
    var worker = CodeHeroMainDetailWorker()
    
    func getCharacter(request: CodeHeroMainDetail.MarvelCharacterSave.Request) {
        if let persona = persona {
            presenter?.presentCharacterSelected(response: CodeHeroMainDetail.MarvelCharacterSave.Response.Success(character: persona))
        }
    }
  
    
}
