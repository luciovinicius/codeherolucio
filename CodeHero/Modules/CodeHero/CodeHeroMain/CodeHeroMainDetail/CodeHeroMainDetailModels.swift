//
//  CodeHeroMainDetailModels.swift
//  CodeHero
//
//  Created by Lucio Couto on 28/09/19.
//  Copyright (c) 2019 Lucio Couto. All rights reserved.
//

import UIKit


enum CodeHeroMainDetail {
  
    enum MarvelCharacterSave {
        
        struct Request {
        }
        
        enum Response {
            struct Error {
            }
            
            struct Success {
                var character: MarvelPersona
            }
        }
        enum ViewModel {
            struct Error {
            }
            struct Success {
                var character: MarvelPersona
            }
        }
    }
}
