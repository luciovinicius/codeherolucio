//
//  CodeHeroMainDetailViewController.swift
//  CodeHero
//
//  Created by Lucio Couto on 28/09/19.
//  Copyright (c) 2019 Lucio Couto. All rights reserved.
//

import UIKit


protocol CodeHeroMainDetailDisplayLogic: class {
    func displayCharacterSelected(viewModel: CodeHeroMainDetail.MarvelCharacterSave.ViewModel.Success)
}


class CodeHeroMainDetailViewController: UIViewController {
  
    @IBOutlet weak var detailImage: UIImageView!
    @IBOutlet weak var detailTitle: UILabel!
    @IBOutlet weak var detailDescription: UILabel!
    
    // Var's
    var interactor: CodeHeroMainDetailBusinessLogic?
    var router: (NSObjectProtocol & CodeHeroMainDetailRoutingLogic & CodeHeroMainDetailDataPassing)?

  
    // Constructor
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
  
  
    // Load
    override func viewDidLoad() {
        super.viewDidLoad()
    }
  
    
    // Appear
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor?.getCharacter(request: CodeHeroMainDetail.MarvelCharacterSave.Request())
    }
    
    
}
