//
//  CodeHeroMainDetailViewController+DisplayLogic.swift
//  CodeHero
//
//  Created by Lucio Couto on 28/09/19.
//  Copyright © 2019 Lucio Couto. All rights reserved.
//

import UIKit

extension CodeHeroMainDetailViewController: CodeHeroMainDetailDisplayLogic {
    
    func displayCharacterSelected(viewModel: CodeHeroMainDetail.MarvelCharacterSave.ViewModel.Success) {
        let url = URL(string: viewModel.character.thumbnail.urlLarge)
        detailImage.sd_setImage(with: url)
        detailTitle.text = viewModel.character.name
        detailDescription.text = viewModel.character.marvelPersonaDescription
    }
    
}
