//
//  CodeHeroMainDetailRouter.swift
//  CodeHero
//
//  Created by Lucio Couto on 28/09/19.
//  Copyright (c) 2019 Lucio Couto. All rights reserved.
//

import UIKit


@objc protocol CodeHeroMainDetailRoutingLogic {
    
}


protocol CodeHeroMainDetailDataPassing {
    var dataStore: CodeHeroMainDetailDataStore? { get }
}


class CodeHeroMainDetailRouter: NSObject, CodeHeroMainDetailRoutingLogic, CodeHeroMainDetailDataPassing {
    
    
    // Var's
    weak var viewController: CodeHeroMainDetailViewController?
    var dataStore: CodeHeroMainDetailDataStore?
  
    
}
