//
//  CodeHeroMainDetailPresenter.swift
//  CodeHero
//
//  Created by Lucio Couto on 28/09/19.
//  Copyright (c) 2019 Lucio Couto. All rights reserved.
//

import UIKit


protocol CodeHeroMainDetailPresentationLogic {
    func presentCharacterSelected(response: CodeHeroMainDetail.MarvelCharacterSave.Response.Success)
}


class CodeHeroMainDetailPresenter: CodeHeroMainDetailPresentationLogic {
  
    
    // Var's
    weak var viewController: CodeHeroMainDetailDisplayLogic?
    
    func presentCharacterSelected(response: CodeHeroMainDetail.MarvelCharacterSave.Response.Success) {
        
        let viewModel = CodeHeroMainDetail.MarvelCharacterSave.ViewModel.Success(character: response.character)
        viewController?.displayCharacterSelected(viewModel: viewModel)
    }
  
}
