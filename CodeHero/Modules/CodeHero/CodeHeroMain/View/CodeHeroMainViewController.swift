//
//  CodeHeroMainViewController.swift
//  CodeHero
//
//  Created by Lucio Couto on 24/09/19.
//  Copyright (c) 2019 Lucio Couto. All rights reserved.
//

import UIKit


protocol CodeHeroMainDisplayLogic: class {
    func displayCharacters(viewModel: CodeHeroMain.MarvelCharacter.ViewModel.Success)
    func displayCharacters(viewModel: CodeHeroMain.MarvelCharacter.ViewModel.Error)
}


class CodeHeroMainViewController: UIViewController {
    
    @IBOutlet weak var marvelTableView: UITableView!
    @IBOutlet weak var marvelCollectionView: UICollectionView!
    @IBOutlet weak var searchTextField: UITextField!
    
    // Var's
    var interactor: CodeHeroMainBusinessLogic?
    var router: (NSObjectProtocol & CodeHeroMainRoutingLogic & CodeHeroMainDataPassing)?
    
    var charactersMarvel: [MarvelPersona]?
    var totalPages: Int = 1
    var pageSelected: Int = 1
    var textToSearch: String = ""
    
    enum SegueIdentifiers: String {
        case presentDetails
    }
    
    // Constructor
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
  
  
    // Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)),
        for: UIControl.Event.editingChanged)
    }
  
    
    // Appear
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor?.getCharacters(request: CodeHeroMain.MarvelCharacter.Request(page: 0, name: textToSearch))
    }
    
    @IBAction func backAction(_ sender: Any) {
        if pageSelected > 1 {
            interactor?.getCharacters(request: CodeHeroMain.MarvelCharacter.Request(page: pageSelected - 2, name: textToSearch))
        }
    }
    
    @IBAction func fowardAction(_ sender: Any) {
        interactor?.getCharacters(request: CodeHeroMain.MarvelCharacter.Request(page: pageSelected, name: textToSearch))
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        textToSearch = searchTextField.text ?? ""
        interactor?.getCharacters(request: CodeHeroMain.MarvelCharacter.Request(page: 0, name: textToSearch))
    }
}
