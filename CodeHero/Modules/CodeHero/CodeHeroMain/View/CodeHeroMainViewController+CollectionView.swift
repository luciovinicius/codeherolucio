//
//  CodeHeroMainViewController+CollectionView.swift
//  CodeHero
//
//  Created by Lucio Couto on 28/09/19.
//  Copyright © 2019 Lucio Couto. All rights reserved.
//

import UIKit

extension CodeHeroMainViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return totalPages
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return configPageCell(indexPath: indexPath, collectionView: collectionView) ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.interactor?.getCharacters(request: CodeHeroMain.MarvelCharacter.Request(page: indexPath.row, name: textToSearch))
    }
    
    func configPageCell(indexPath: IndexPath, collectionView: UICollectionView) -> PaginatedCell? {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "circleCell", for: indexPath) as? PaginatedCell {
            cell.numberLabel.text = "\(indexPath.row + 1)"
            cell.numberLabel.layer.cornerRadius = cell.numberLabel.frame.width/2
            cell.numberLabel.layer.masksToBounds = true
            cell.numberLabel.layer.borderColor = #colorLiteral(red: 0.8728258014, green: 0.2138626277, blue: 0.1941210032, alpha: 1)
            cell.numberLabel.layer.borderWidth = 1
            
            if (indexPath.row + 1) == pageSelected {
                cell.numberLabel.textColor = .white
                cell.numberLabel.backgroundColor = #colorLiteral(red: 0.8728258014, green: 0.2138626277, blue: 0.1941210032, alpha: 1)
            } else {
                cell.numberLabel.textColor = #colorLiteral(red: 0.8728258014, green: 0.2138626277, blue: 0.1941210032, alpha: 1)
                cell.numberLabel.backgroundColor = .white
            }
            return cell
        }
        return nil
    }
    
}

class PaginatedCell: UICollectionViewCell {
    
    @IBOutlet var numberLabel: UILabel!
}
