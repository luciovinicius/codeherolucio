//
//  CodeHeroMainViewController+TableView.swift
//  CodeHero
//
//  Created by Lucio Couto on 26/09/19.
//  Copyright © 2019 Lucio Couto. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

extension CodeHeroMainViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return charactersMarvel?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return configCell(indexPath: indexPath) ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let persona = charactersMarvel?[indexPath.row] {
            let request = CodeHeroMain.MarvelCharacterSave.Request(character: persona)
            interactor?.saveCharacter(request: request)
            performSegue(withIdentifier: SegueIdentifiers.presentDetails.rawValue, sender: nil)
        }
        
    }
    
    func configCell(indexPath: IndexPath) -> CodeHeroCharCell? {
        if let cell = marvelTableView.dequeueReusableCell(withIdentifier: "marvelCell") as? CodeHeroCharCell {
            cell.personName.text = charactersMarvel?[indexPath.row].name ?? ""
            let url = URL(string: charactersMarvel?[indexPath.row].thumbnail.urlMedium ?? "")
            cell.personImage.sd_setImage(with: url)
            cell.personImage.maskCircle(anyImage: cell.personImage.image ?? UIImage())
            return cell
        }
        return nil
    }

}

class CodeHeroCharCell: UITableViewCell {
    
    @IBOutlet weak var personImage: UIImageView!
    @IBOutlet weak var personName: UILabel!
}

