//
//  CodeHeroMainViewController+DisplayLogic.swift
//  CodeHero
//
//  Created by Lucio Couto on 26/09/19.
//  Copyright © 2019 Lucio Couto. All rights reserved.
//

import Foundation

extension CodeHeroMainViewController: CodeHeroMainDisplayLogic {
    
    func displayCharacters(viewModel: CodeHeroMain.MarvelCharacter.ViewModel.Success) {
        charactersMarvel = viewModel.characters
        pageSelected = viewModel.pageSelected
        totalPages = viewModel.totalPages
        marvelTableView.reloadData()
        marvelCollectionView.reloadData()
        marvelCollectionView.scrollToItem(at: IndexPath(row: pageSelected - 1, section: 0), at: [.centeredVertically, .centeredHorizontally], animated: true)
    }
    
    func displayCharacters(viewModel: CodeHeroMain.MarvelCharacter.ViewModel.Error) {
        charactersMarvel = [MarvelPersona]()
        pageSelected = 1
        totalPages = 1
        marvelTableView.reloadData()
        marvelCollectionView.reloadData()
        marvelCollectionView.scrollToItem(at: IndexPath(row: pageSelected - 1, section: 0), at: [.centeredVertically, .centeredHorizontally], animated: true)
    }
}
