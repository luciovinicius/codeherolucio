//
//  UIImageViewExtension.swift
//  CodeHero
//
//  Created by Lucio Couto on 26/09/19.
//  Copyright © 2019 Lucio Couto. All rights reserved.
//

import UIKit

extension UIImageView {
    
  public func maskCircle(anyImage: UIImage) {
    self.contentMode = UIView.ContentMode.scaleAspectFill
    self.layer.cornerRadius = self.frame.height / 2
    self.layer.masksToBounds = false
    self.clipsToBounds = true
    self.image = anyImage
  }
}
